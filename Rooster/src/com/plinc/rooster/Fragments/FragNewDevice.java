package com.plinc.rooster.Fragments;

import com.freand.rooster.R;
import com.plinc.rooster.MainActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

public class FragNewDevice extends DialogFragment {

	private EditText etIp;
	private EditText etMac;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.layout_frag_new_device, null);

		etIp = (EditText) view.findViewById(R.id.etIp);
		etMac = (EditText) view.findViewById(R.id.etMac);

		AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity())
				.setView(view)
				.setTitle(R.string.add_device)
				.setPositiveButton(R.string.button_positive,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								String IP = etIp.getText().toString();
								String MAC = etMac.getText().toString();

								((MainActivity) getActivity()).addNewDevice(IP,
										MAC);
								dialog.dismiss();
							}
						})

				.setNegativeButton(R.string.button_negative,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int whichButton) {
								dialog.dismiss();
							}
						});

		return dialog.create();
	}

}

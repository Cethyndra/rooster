package com.plinc.rooster;

import java.util.ArrayList;

import com.freand.rooster.R;
import com.plinc.rooster.Entities.Device;
import com.plinc.rooster.Fragments.FragNewDevice;
import com.plinc.rooster.Tools.FileHandler;
import com.plinc.rooster.Tools.WakeOnLan;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Rooster
 * 
 * This application sends a Wake-on-Lan packet to the device with the specified
 * address.
 * 
 * @author Fredrik Andersson
 * 
 */
public class MainActivity extends Activity {

	private ArrayAdapter<String> arrayAdapter;
	private ArrayList<Device> devices;
	private EditText etIP, etMac;
	private ListView lvDevices;
	private WakeOnLan wol;
	private FileHandler fileHandler;

	private int listPosition;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		wol = new WakeOnLan(this);
		fileHandler = new FileHandler(getString(R.string.app_name));

		readLocalDevices();
		initGuiComponents();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_activity_actions, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_add_device:
			new FragNewDevice().show(getFragmentManager(), "dialog");
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * Initializes the GUI-components.
	 */
	private void initGuiComponents() {
		lvDevices = (ListView) findViewById(R.id.lvDevices);
		lvDevices.setOnItemClickListener(new CustomOnClickListener());
		arrayAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, getDevices());
		lvDevices.setAdapter(arrayAdapter);
	}

	/**
	 * Returns the list of current Device-objects.
	 * 
	 * @return The list of current Device-objects.
	 */
	public ArrayList getDevices() {
		return devices;
	}

	/**
	 * Adds a new Device to the list.
	 * 
	 * @param view
	 */
	public void addNewDevice(String IP, String MAC) {
		devices.add(new Device(IP, MAC, ""));
		Toast.makeText(this, R.string.add_success, Toast.LENGTH_SHORT).show();
		backupDevices();
	}

	/**
	 * Removes the specified Device from the list.
	 */
	public void removeDevice() {
		devices.remove(listPosition);
		Toast.makeText(this, R.string.remove_success, Toast.LENGTH_SHORT)
				.show();
	}

	/**
	 * Creates a String containing the IP and MAC for all Device-objects in the
	 * list, and writes them t
	 */
	private void backupDevices() {
		String str = "";
		for (int i = 0; i < devices.size(); i++) {
			str += devices.get(i).getIp();
			str += ";";
			str += devices.get(i).getMac();
			str += ";";
		}

		fileHandler.writeToFile("devicelist.dat", str);
	}

	/**
	 * Reads the IP- and MAC-addresses from file, and populates the list with
	 * Device-objects.
	 */
	public void readLocalDevices() {
		ArrayList<String> deviceString = fileHandler
				.readFromFile("devicelist.dat");

		devices = new ArrayList<Device>();

		if (deviceString.size() != 0) {

			String[] str = deviceString.get(0).split(";");

			for (int i = 0; i < str.length - 1; i += 2) {
				devices.add(new Device(str[i], str[i + 1], ""));
			}
		}
	}

	/**
	 * Starts the sending of the WoL-packet to the specified address.
	 */
	public void sendWakeUp() {
		String IP = devices.get(listPosition).getIp();
		String MAC = devices.get(listPosition).getMac();

		wol.execute(IP, MAC);
	}

	/**
	 * Displays a dialog for the selected Device in the list.
	 */
	private void showSelectionDialog() {
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();

		alertDialog.setMessage(devices.get(listPosition).getIp());

		alertDialog.setButton(-1, "Wake up",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.hide();
						sendWakeUp();
					}
				});

		alertDialog.setButton(-2, "Delete",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.hide();
						removeDevice();
						backupDevices();
						arrayAdapter.notifyDataSetChanged();
					}
				});
		
		alertDialog.show();
	}

	/**
	 * A custom OnItemClickListener for the ListView.
	 * 
	 * @author Fredrik Andersson
	 * 
	 */
	private class CustomOnClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position,
				long arg3) {

			listPosition = position;
			showSelectionDialog();
		}
	};
}

package com.plinc.rooster.Tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.os.Environment;
import android.util.Log;

/**
 * This class handles the reading and writing of files to the SD card of the
 * Android unit.
 * 
 * @author Fredrik Andersson
 * 
 */

public class FileHandler {

	private File sdCardFolder;
	private final String tag = "Rooster | FileHandler";

	/**
	 * A basic constructor, which sets the path where files should be placed.
	 */
	public FileHandler(String folderName) {
		this.sdCardFolder = new File(Environment.getExternalStorageDirectory(),
				folderName);
	}

	/**
	 * Reads a file from the SD card and returns the content in the form of an
	 * ArrayList. If the file doesn't exist this method creates it.
	 * 
	 * @param fileName
	 *            The name of the file (eg. 'clientlist.txt')
	 * @return The content of the file as an ArrayList<String>.
	 */
	public ArrayList<String> readFromFile(String fileName) {

		ArrayList<String> stringsFromFile = new ArrayList<String>();
		BufferedReader reader = null;
		String str;

		try {
			File file = new File(sdCardFolder, fileName);

			if (!sdCardFolder.exists()) {
				sdCardFolder.mkdir();
			}

			if (!file.exists()) {
				try {
					Log.i(tag, "Create: " + file.createNewFile());
				} catch (IOException e) {
					Log.i(tag, "Unable to create file: " + fileName);
				}
			} else {

				reader = new BufferedReader(new InputStreamReader(
						new FileInputStream(file), "UTF-8"));
				while ((str = reader.readLine()) != null) {
					stringsFromFile.add(str);
					Log.i(tag, "Read from file: " + str);
				}
			}

		} catch (IOException e) {
			Log.i(tag, "Error while reading file: " + fileName);
		}

		return stringsFromFile;

	}

	/**
	 * Writes to a file on the SD card.
	 * 
	 * @param fileName
	 *            The name of the file (eg. 'clientlist.dat')
	 * @param fileBody
	 *            The content to be written to the file.
	 */
	public void writeToFile(String fileName, String fileBody) {

		try {
			File file = new File(sdCardFolder, fileName);
			FileWriter writer = new FileWriter(file);
			writer.append(fileBody);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			Log.i(tag, "Unable to write to file: " + fileName);
		}
	}
}

package com.plinc.rooster.Entities;

public class Device {
	
	String ip, mac, name;
	
	public Device(String aIp, String aMac, String aName){
		ip = aIp;
		mac = aMac;
		name = aName;
	}
	
	public String getIp(){
		return ip;
	}
	
	public String getMac(){
		return mac;
	}
	
	public String getName(){
		return name;
	}
	
	public void setIp(String ip) {
		this.ip = ip;
	}

	public void setMac(String mac) {
		this.mac = mac;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString(){
		return ip;
	}
}
